Este desarrollo solo es un ejemplo de ZyncroApp, un pequeño desarrollo que se ejecutaba en la plataforma colaborativa Zyncro
y que permitia añadir nuevas funcionalidades a la web.

Estas ZyncroApps al principio eran simplemente archivos de javascript que modificaban la página web pero me encargaron crear
un Framework que permitiese crear Apps más complejas y útiles. Como resultado obtuvimos este tipo de desarrollo.

El código es solo un ejemplo y lo publiqué en 2013, en el repositorio público de Zyncro por lo que no tiene derechos de autor.